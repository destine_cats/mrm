#!/bin/bash
#  --------------------------------------------------
#  File:        concatenate_discharge.sh
# 
#  Created:     Thu 01-10-2020
#  Author:      Stephan Thober
# 
#  --------------------------------------------------
# 
#  Description: merge all discharge ascii files together
# 
#  Modified:
# 
#  --------------------------------------------------
set -e
# set -x

suite_root='/scratch/mo/nest/ulysses/operational_seasonal_forecast/ecflow_work/mhm_reference_run_v3/'
hm='mhm'
suite_root='/scratch/mo/nest/ulysses/operational_seasonal_forecast/ecflow_work/jules_mrm_reference_run/'
hm='jules'
n_domains=53
y_start=1981
y_end=2019
y_end=2010
outpath=${suite_root}'/discharge_files/'${hm}'/'

if [ ! -d ${outpath} ]; then mkdir -p ${outpath}; fi

for ii in $(seq ${n_domains}); do
    echo 'processing subdomain '${ii}
    # check whether there are some gauges
    if [ ! -f "${suite_root}/mrm_${hm}/m00/subdomain_${ii}/198101/output/daily_discharge.out" ]; then echo 'no gauge in subdomain '${ii}; continue; fi
    # outfile
    outfile=${outpath}'/subdmoain'${ii}'_daily_discharge.out'
    if [ -f ${outfile} ]; then rm ${outfile}; fi
    # path to discharge files
    # /scratch/mo/nest/ulysses/operational_seasonal_forecast/ecflow_work/mhm_reference_run_v2/mrm_mhm/m00/subdomain_1/198101/output
    for yy in $(seq ${y_start} ${y_end}); do
        for mm in 01 02 03 04 05 06 07 08 09 10 11 12; do
            tmpfile="${suite_root}/mrm_${hm}/m00/subdomain_${ii}/${yy}${mm}/output/daily_discharge.out"
            # paste everything but not first line
            if [ ! -f ${outfile} ]; then
                cat ${tmpfile} >> ${outfile}
            else
                tail -n +2 ${tmpfile} >> ${outfile}
            fi
        done
    done
done

echo 'Done!'
