!>       \file mo_mrm_read_data.f90

!>       \brief This module contains all routines to read mRM data from file.
!>       \details

!>       \details TODO: add description

!>       \authors Stephan Thober

!>       \date Aug 2015

! Modifications:

module mo_mrm_read_data
  use mo_kind, only : i4, dp
  implicit none
  public :: mrm_read_L0_data
  public :: mrm_read_discharge
  public :: mrm_read_total_runoff
  public :: mrm_read_bankfull_runoff
  private
contains
  ! ------------------------------------------------------------------

  !    NAME
  !        mrm_read_L0_data

  !    PURPOSE
  !>       \brief read L0 data from file

  !>       \details With the exception of L0_mask, L0_elev, and L0_LCover, all
  !>       L0 variables are read from file. The former three are only read if they
  !>       are not provided as variables.

  !    INTENT(IN)
  !>       \param[in] "logical :: do_reinit"
  !>       \param[in] "logical :: do_readlatlon"
  !>       \param[in] "logical :: do_readlcover"

  !    HISTORY
  !>       \authors Juliane Mai, Matthias Zink, and Stephan Thober

  !>       \date Aug 2015

  ! Modifications:
  ! Stephan Thober Sep 2015 - added L0_mask, L0_elev, and L0_LCover
  ! Robert Schweppe Jun 2018 - refactoring and reformatting
  ! Stephan Thober Jun 2018 - including varying celerity functionality

  subroutine mrm_read_L0_data(do_reinit, do_readlatlon, do_readlcover)

    use mo_append, only : append
    use mo_common_constants, only : nodata_i4
#ifndef MRM2MHM
    use mo_common_constants, only : nodata_dp
    use mo_mpr_global_variables, only: L0_slope
#endif
    use mo_common_read_data, only : read_dem, read_lcover
    use mo_common_variables, only : Grid, L0_LCover, dirMorpho, level0, domainMeta, processMatrix
    use mo_mpr_file, only: file_slope, uslope
    use mo_message, only : message
    use mo_mrm_file, only : file_facc, file_fdir, &
                            file_gaugeloc, ufacc, ufdir, ugaugeloc
    use mo_mrm_global_variables, only : L0_InflowGaugeLoc, L0_fAcc, L0_fDir, L0_gaugeLoc, domain_mrm
    use mo_read_spatial_data, only : read_spatial_data_ascii
    use mo_string_utils, only : num2str
    use mo_netcdf, only: NcDataset, NcVariable

    implicit none

    logical, intent(in) :: do_reinit

    logical, intent(in) :: do_readlatlon

    logical, intent(in) :: do_readlcover

    integer(i4) ::domainID, iDomain

    integer(i4) :: iVar

    integer(i4) :: iGauge

    character(256) :: fname

    integer(i4) :: nunit

    integer(i4), dimension(:, :), allocatable :: data_i4_2d
    integer(i4), dimension(:, :), allocatable :: data_i4_2d_tmp

    real(dp), dimension(:, :), allocatable :: data_dp_2d
    real(dp), dimension(:, :), allocatable :: data_dp_2d_tmp

    integer(i4), dimension(:, :), allocatable :: dataMatrix_i4

    logical, dimension(:, :), allocatable :: mask_2d

    logical, dimension(:, :), allocatable :: mask_global

    type(Grid), pointer :: level0_iDomain => null()

    type(NcDataset) :: nc_in
    type(NcVariable) :: nc_var
    logical, dimension(:, :), allocatable :: zeros, minusones, nines
    logical :: isYReverse

    ! ************************************************
    ! READ SPATIAL DATA FOR EACH DOMAIN
    ! ************************************************
    if (do_reinit) then
      print *, 'read dem...'
      call read_dem(isYReverse)
    end if

    if (do_readlcover .and. processMatrix(8, 1) .eq. 1) then
      call read_lcover()
    else if (do_readlcover .and. ((processMatrix(8, 1) .eq. 2) .or. (processMatrix(8, 1) .eq. 3))) then
      allocate(dataMatrix_i4(count(mask_global), 1))
      dataMatrix_i4 = nodata_i4
      call append(L0_LCover, dataMatrix_i4)
      ! free memory
      deallocate(dataMatrix_i4)
    end if

    do iDomain = 1, domainMeta%nDomains
      domainID = domainMeta%indices(iDomain)

      level0_iDomain => level0(domainMeta%L0DataFrom(iDomain))

      ! ToDo: check if change is correct
      ! check whether L0 data is shared
      if (domainMeta%L0DataFrom(iDomain) < iDomain) then
        !
        call message('      Using data of domain ', &
                trim(adjustl(num2str(domainMeta%indices(domainMeta%L0DataFrom(iDomain))))), ' for domain: ',&
                trim(adjustl(num2str(domainID))), '...')
        cycle
        !
      end if
      !
      call message('      Reading data for domain: ', trim(adjustl(num2str(domainID))), ' ...')

      ! read fAcc, fDir, gaugeLoc
      do iVar = 1, 4
        select case (iVar)
        case(1) ! flow accumulation
          fName = trim(adjustl(dirMorpho(iDomain))) ! // trim('river_network.nc')
          nunit = ufacc
        case(2) ! flow direction
           fName = trim(adjustl(dirMorpho(iDomain))) !gg // trim('river_network.nc')
          nunit = ufdir
        case(3) ! location of gauging stations
          fName = trim(adjustl(dirMorpho(iDomain))) // trim(adjustl(file_gaugeloc))
          nunit = ugaugeloc
       case(4)  ! slope
          fName = trim(adjustl(dirMorpho(iDomain))) // trim(adjustl(file_slope))
          nunit = uslope
       end select

       if (iVar .le. 2) then
          !
          ! ! reading and transposing
          ! call read_spatial_data_ascii(trim(fName), nunit, &
          !      level0_iDomain%nrows, level0_iDomain%ncols, level0_iDomain%xllcorner, &
          !      level0_iDomain%yllcorner, level0_iDomain%cellsize, data_i4_2d, mask_2d)
          nc_in = NcDataset(trim(fname), 'r')
          if (iVar .eq. 2) then
            print *, 'reading flow dir'
            nc_var = nc_in%getVariable('flwdir')
            call nc_var%getData(data_i4_2d)
            data_i4_2d_tmp = transpose(data_i4_2d)
            call move_alloc(data_i4_2d_tmp, data_i4_2d)

             zeros = data_i4_2d .eq. 0_i4
             minusones = data_i4_2d .eq. -1_i4
             nines = data_i4_2d .eq. 9_i4
             print *, 'number of cells with zeros: ', count(zeros)
             print *, 'number of cells with -ones: ', count(minusones)
             print *, 'number of cells with nines: ', count(nines)
             if (count(nines) > 0_i4 .or. count(zeros) == 0_i4) then
              print *, ''
              print *, 'convert fDir to mRM convention'
              print *, '   - changing convention from 1..9 to 1..128'
              print *, '   - set inland sinks (5) to sinks 0'
              data_i4_2d_tmp = data_i4_2d
              data_i4_2d_tmp = merge(8_i4, data_i4_2d_tmp, (data_i4_2d .eq. 1_i4))
              data_i4_2d_tmp = merge(4_i4, data_i4_2d_tmp, (data_i4_2d .eq. 2_i4))
              data_i4_2d_tmp = merge(2_i4, data_i4_2d_tmp, (data_i4_2d .eq. 3_i4))
              data_i4_2d_tmp = merge(16_i4, data_i4_2d_tmp, (data_i4_2d .eq. 4_i4))
              data_i4_2d_tmp = merge(0_i4, data_i4_2d_tmp, (data_i4_2d .eq. 5_i4))
              data_i4_2d_tmp = merge(1_i4, data_i4_2d_tmp, (data_i4_2d .eq. 6_i4))
              data_i4_2d_tmp = merge(32_i4, data_i4_2d_tmp, (data_i4_2d .eq. 7_i4))
              data_i4_2d_tmp = merge(64_i4, data_i4_2d_tmp, (data_i4_2d .eq. 8_i4))
              data_i4_2d_tmp = merge(128_i4, data_i4_2d_tmp, (data_i4_2d .eq. 9_i4))
              call move_alloc(data_i4_2d_tmp, data_i4_2d)
              print *, 'number of cells with zeros: ', count(data_i4_2d .eq. 0)
              print *, '' 
             else
              print *, ''
              print *, 'convert fDir to mRM convention'
              print *, '   - changing convention from 1..8 to 1..128' ! see http://hydro.iis.u-tokyo.ac.jp/~yamadai/flow/tech.html
              print *, '   - set inland sinks (-1) to sinks 0'
              ! data_i4_2d = merge(2**mod(data_i4_2d - 3, 8), nodata_i4, level0_iDomain%mask)
              ! print *, '1 ', count(data_i4_2d .eq. 1)
              ! print *, '2 ', count(data_i4_2d .eq. 2)
              ! print *, '3 ', count(data_i4_2d .eq. 3)
              ! print *, '4 ', count(data_i4_2d .eq. 4)
              ! print *, '5 ', count(data_i4_2d .eq. 5)
              ! print *, '6 ', count(data_i4_2d .eq. 6)
              ! print *, '7 ', count(data_i4_2d .eq. 7)
              ! print *, '8: ', count(data_i4_2d .eq.8)
              data_i4_2d = merge(2**abs(mod(data_i4_2d + 5, 8)), data_i4_2d, (data_i4_2d .ge. 1))
              ! print *, '3 -> 1 : ', count(data_i4_2d .eq. 1 ),   abs(mod(3+5, 8)), 2**abs(mod(3+5, 8))
              ! print *, '4 -> 2 : ', count(data_i4_2d .eq. 2 ),   abs(mod(4+5, 8)), 2**abs(mod(4+5, 8))
              ! print *, '5 -> 4 : ', count(data_i4_2d .eq. 4 ),   abs(mod(5+5, 8)), 2**abs(mod(5+5, 8))
              ! print *, '6 -> 8 : ', count(data_i4_2d .eq. 8 ),   abs(mod(6+5, 8)), 2**abs(mod(6+5, 8))
              ! print *, '7 -> 16: ', count(data_i4_2d .eq. 16),   abs(mod(7+5, 8)), 2**abs(mod(7+5, 8))
              ! print *, '8 -> 32: ', count(data_i4_2d .eq. 32),   abs(mod(8+5, 8)), 2**abs(mod(8+5, 8))
              ! print *, '1 -> 64: ', count(data_i4_2d .eq. 64),   abs(mod(1+5, 8)), 2**abs(mod(1+5, 8))
              ! print *, '2 -> 128: ', count(data_i4_2d .eq. 128), abs(mod(2+5, 8)), 2**abs(mod(2+5, 8))
              ! print *, data_i4_2d(321, 1895), shape(data_i4_2d)
              
              ! data_i4_2d_tmp = data_i4_2d
              ! data_i4_2d_tmp = merge(8_i4, data_i4_2d_tmp, (data_i4_2d .eq. 32_i4))
              ! data_i4_2d_tmp = merge(4_i4, data_i4_2d_tmp, (data_i4_2d .eq. 64_i4))
              ! data_i4_2d_tmp = merge(2_i4, data_i4_2d_tmp, (data_i4_2d .eq. 128_i4))
              ! data_i4_2d_tmp = merge(32_i4, data_i4_2d_tmp, (data_i4_2d .eq. 8_i4))
              ! data_i4_2d_tmp = merge(64_i4, data_i4_2d_tmp, (data_i4_2d .eq. 4_i4))
              ! data_i4_2d_tmp = merge(128_i4, data_i4_2d_tmp, (data_i4_2d .eq. 2_i4))
              ! call move_alloc(data_i4_2d_tmp, data_i4_2d)

              print *, 'number of cells with -ones (before reset): ', count(data_i4_2d .eq. -1)
              print *, '' 
              data_i4_2d = merge(0, data_i4_2d, (data_i4_2d .eq. -1))
              print *, 'number of cells with zeros: ', count(data_i4_2d .eq. 0)
             end if
            elseif (iVar .eq. 1) then
             print *, 'reading flow acc'
             nc_var = nc_in%getVariable('uparea_grid')
             call nc_var%getData(data_dp_2d)
             data_dp_2d_tmp = transpose(data_dp_2d)
             call move_alloc(data_dp_2d_tmp, data_dp_2d)
              data_dp_2d = merge(nodata_dp, data_dp_2d * 1e-6, data_dp_2d .lt. 0)
          end if
          call nc_in%close()

       end if

       if (iVar .eq. 3) then
          print *, 'nGauges: ', domain_mrm(iDomain)%nGauges
          if (domain_mrm(iDomain)%nGauges .gt. 0) then
             ! reading and transposing
             ! print *, 'nrows: ', level0_iDomain%nrows
             ! print *, 'ncols: ', level0_iDomain%ncols
             ! print *, 'csize: ', level0_iDomain%cellsize
             ! print *, 'xllcorner: ', level0_iDomain%xllcorner
             ! print *, 'yllcorner: ', level0_iDomain%yllcorner
             ! HACK TO AVOID BUG (ncols and nrows interchanged)
             call read_spatial_data_ascii(trim(fName), nunit, &
                  level0_iDomain%ncols, level0_iDomain%nrows, level0_iDomain%xllcorner, &
                  level0_iDomain%yllcorner, level0_iDomain%cellsize, data_i4_2d, mask_2d)
             data_i4_2d = merge(data_i4_2d, nodata_i4, mask_2d)
             ! For ULYSSES, data needs to be in (nrow, ncol)
             data_i4_2d_tmp = transpose(data_i4_2d)
             call move_alloc(data_i4_2d_tmp, data_i4_2d) 
          else
             if (.not. allocated(data_i4_2d)) allocate(data_i4_2d(level0_iDomain%nrows, level0_iDomain%ncols))
             data_i4_2d = nodata_i4
          end if
       end if

#ifndef MRM2MHM
       ! only read slope if not coupled to mHM and using third process Matrix
       if ((iVar .eq. 4) .and. (processMatrix(8, 1) .eq. 3)) then
          ! reading
          ! HACK TO AVOID BUG (ncols and nrows interchanged)
          call read_spatial_data_ascii(trim(fName), nunit, &
               level0_iDomain%ncols, level0_iDomain%nrows, level0_iDomain%xllcorner, &
               level0_iDomain%yllcorner, level0_iDomain%cellsize, data_dp_2d, mask_2d)
          ! put global nodata value into array (probably not all grid cells have values)
          data_dp_2d = merge(data_dp_2d, nodata_dp, mask_2d)
          ! put data in variable
          call append(L0_slope, pack(data_dp_2d, level0_iDomain%mask))

          ! deallocate arrays
          deallocate(data_dp_2d, mask_2d)
       end if
#endif

        ! put data into global L0 variable
        select case (iVar)
        case(1) ! flow accumulation
          call append(L0_fAcc, pack(nint(data_dp_2d), level0_iDomain%mask))
        case(2) ! flow direction
          ! rotate flow direction and any other variable with directions
          ! NOTE: ONLY when ASCII files are read
          ! call rotate_fdir_variable(data_i4_2d)
          ! append
           ! print *, '#cells with valid fdir: ', count(data_i4_2d .ge. 0)
           ! print *, '#cells in mask:         ', count(level0_iDomain%mask)
           ! stop 'TESTING'
          call append(L0_fDir, pack(data_i4_2d, level0_iDomain%mask))
        case(3) ! location of evaluation and inflow gauging stations
          ! evaluation gauges
          ! Input data check
          do iGauge = 1, domain_mrm(iDomain)%nGauges
            ! If gaugeId is found in gauging location file?
            if (.not. any(data_i4_2d .EQ. domain_mrm(iDomain)%gaugeIdList(iGauge))) then
              call message()
              call message('***ERROR: Gauge ID "', trim(adjustl(num2str(domain_mrm(iDomain)%gaugeIdList(iGauge)))), &
                      '" not found in ')
              call message('          Gauge location input file: ', &
                      trim(adjustl(dirMorpho(iDomain))) // trim(adjustl(file_gaugeloc)))
              stop
            end if
          end do

          call append(L0_gaugeLoc, pack(data_i4_2d, level0_iDomain%mask))

          ! inflow gauges
          ! if no inflow gauge for this subdomain exists still matirx with dim of subdomain has to be paded
          if (domain_mrm(iDomain)%nInflowGauges .GT. 0_i4) then
            ! Input data check
            do iGauge = 1, domain_mrm(iDomain)%nInflowGauges
              ! If InflowGaugeId is found in gauging location file?
              if (.not. any(data_i4_2d .EQ. domain_mrm(iDomain)%InflowGaugeIdList(iGauge))) then
                call message()
                call message('***ERROR: Inflow Gauge ID "', &
                        trim(adjustl(num2str(domain_mrm(iDomain)%InflowGaugeIdList(iGauge)))), &
                        '" not found in ')
                call message('          Gauge location input file: ', &
                        trim(adjustl(dirMorpho(iDomain))) // trim(adjustl(file_gaugeloc)))
                stop 1
              end if
            end do
          end if

          call append(L0_InflowGaugeLoc, pack(data_i4_2d, level0_iDomain%mask))

        end select
        !
        ! deallocate arrays
        if (allocated(data_i4_2d)) deallocate(data_i4_2d)
        if (allocated(mask_2d)) deallocate(mask_2d)
        !
      end do
    end do

  end subroutine mrm_read_L0_data
  ! ---------------------------------------------------------------------------

  !    NAME
  !        mrm_read_discharge

  !    PURPOSE
  !>       \brief Read discharge timeseries from file

  !>       \details Read Observed discharge at the outlet of a catchment
  !>       and at the inflow of a catchment. Allocate global runoff
  !>       variable that contains the simulated runoff after the simulation.

  !    HISTORY
  !>       \authors Matthias Zink & Stephan Thober

  !>       \date Aug 2015

  ! Modifications:
  ! Robert Schweppe Jun 2018 - refactoring and reformatting

  subroutine mrm_read_discharge

    use mo_append, only : paste
    use mo_common_constants, only : nodata_dp
    use mo_common_mHM_mRM_variables, only : evalPer, nTstepDay, opti_function, optimize, simPer
    use mo_common_variables, only : domainMeta
    use mo_message, only : message
    use mo_mrm_file, only : udischarge
    use mo_mrm_global_variables, only : InflowGauge, gauge, mRM_runoff, nGaugesLocal, &
                                        nInflowGaugesTotal, nMeasPerDay
    use mo_read_timeseries, only : read_timeseries
    use mo_string_utils, only : num2str

    implicit none

    integer(i4) :: iGauge

    integer(i4) :: iDomain

    integer(i4) :: maxTimeSteps

    ! file name of file to read
    character(256) :: fName

    integer(i4), dimension(3) :: start_tmp, end_tmp

    real(dp), dimension(:), allocatable :: data_dp_1d

    logical, dimension(:), allocatable :: mask_1d


    !----------------------------------------------------------
    ! INITIALIZE RUNOFF
    !----------------------------------------------------------
    maxTimeSteps = maxval(simPer(1 : domainMeta%nDomains)%julEnd - simPer(1 : domainMeta%nDomains)%julStart + 1) * nTstepDay
    allocate(mRM_runoff(maxTimeSteps, nGaugesLocal))
    mRM_runoff = nodata_dp

    ! READ GAUGE DATA
    do iGauge = 1, nGaugesLocal
      ! get domain id
      iDomain = gauge%domainId(iGauge)
      ! get start and end dates
      start_tmp = (/evalPer(iDomain)%yStart, evalPer(iDomain)%mStart, evalPer(iDomain)%dStart/)
      end_tmp = (/evalPer(iDomain)%yEnd, evalPer(iDomain)%mEnd, evalPer(iDomain)%dEnd  /)
      ! evaluation gauge
      fName = trim(adjustl(gauge%fname(iGauge)))
      call read_timeseries(trim(fName), udischarge, &
              start_tmp, end_tmp, optimize, opti_function, &
              data_dp_1d, mask = mask_1d, nMeasPerDay = nMeasPerDay)
      data_dp_1d = merge(data_dp_1d, nodata_dp, mask_1d)
      call paste(gauge%Q, data_dp_1d, nodata_dp)
      deallocate (data_dp_1d)
    end do
    !
    ! inflow gauge
    !
    ! in mhm call InflowGauge%Q has to be initialized -- dummy allocation with period of domain 1 and initialization
    if (nInflowGaugesTotal .EQ. 0) then
      allocate(data_dp_1d(maxval(simPer(:)%julEnd - simPer(:)%julStart + 1)))
      data_dp_1d = nodata_dp
      call paste(InflowGauge%Q, data_dp_1d, nodata_dp)
    else
      do iGauge = 1, nInflowGaugesTotal
        ! get domain id
        iDomain = InflowGauge%domainId(iGauge)
        ! get start and end dates
        start_tmp = (/simPer(iDomain)%yStart, simPer(iDomain)%mStart, simPer(iDomain)%dStart/)
        end_tmp = (/simPer(iDomain)%yEnd, simPer(iDomain)%mEnd, simPer(iDomain)%dEnd  /)
        ! inflow gauge
        fName = trim(adjustl(InflowGauge%fname(iGauge)))
        call read_timeseries(trim(fName), udischarge, &
                start_tmp, end_tmp, optimize, opti_function, &
                data_dp_1d, mask = mask_1d, nMeasPerDay = nMeasPerDay)
        if (.NOT. (all(mask_1d))) then
          call message()
          call message('***ERROR: Nodata values in inflow gauge time series. File: ', trim(fName))
          call message('          During simulation period from ', num2str(simPer(iDomain)%yStart) &
                  , ' to ', num2str(simPer(iDomain)%yEnd))
          stop
        end if
        data_dp_1d = merge(data_dp_1d, nodata_dp, mask_1d)
        call paste(InflowGauge%Q, data_dp_1d, nodata_dp)
        deallocate (data_dp_1d)
      end do
    end if

  end subroutine mrm_read_discharge

  ! ---------------------------------------------------------------------------

  !    NAME
  !        mrm_read_total_runoff

  !    PURPOSE
  !>       \brief read simulated runoff that is to be routed

  !>       \details read spatio-temporal field of total runoff that has been
  !>       simulated by a hydrologic model or land surface model. This
  !>       total runoff will then be aggregated to the level 11 resolution
  !>       and then routed through the stream network.

  !    INTENT(IN)
  !>       \param[in] "integer(i4) :: iDomain" domain id

  !    HISTORY
  !>       \authors Stephan Thober

  !>       \date Sep 2015

  ! Modifications:
  ! Stephan Thober  Feb 2016 - refactored deallocate statements
  ! Stephan Thober  Sep 2016 - added ALMA convention
  ! Robert Schweppe Jun 2018 - refactoring and reformatting

  subroutine mrm_read_total_runoff(iDomain)

    use mo_append, only : append
    use mo_common_constants, only : HourSecs, nodata_dp
    use mo_common_mHM_mRM_variables, only : simPer, timestep
    use mo_common_variables, only : ALMA_convention, level1
    use mo_mrm_global_variables, only : L1_total_runoff_in, dirTotalRunoff, &
         filenameTotalRunoff, varnameTotalRunoff, &
         filenamePreRunoff, varnamePreRunoff, &
         filenamePETRunoff, varnamePETRunoff

    use mo_read_forcing_nc, only : read_forcing_nc

    implicit none

    ! domain id
    integer(i4), intent(in) :: iDomain

    integer(i4) :: tt

    integer(i4) :: nTimeSteps

    ! tell nc file to read daily or hourly values
    integer(i4) :: nctimestep

    ! read data from file
    real(dp), dimension(:, :, :), allocatable :: L1_data, L1_data_2

    real(dp), dimension(:, :), allocatable :: L1_data_packed
    real(dp),                  allocatable :: L1_data_packed_2(:)

    logical, allocatable :: data_mask(:, :, :), lake_mask(:, :)

    print *, 'mrm_read_total_runoff ...'

    if (timestep .eq. 1) nctimestep = -4 ! hourly input
    if (timestep .eq. 24) nctimestep = -1 ! daily input
    call read_forcing_nc(trim(dirTotalRunoff(iDomain)), level1(iDomain)%nrows, level1(iDomain)%ncols, &
         varnameTotalRunoff, level1(iDomain)%mask, L1_data, maskout = data_mask, &
         target_period = simPer(iDomain), nctimestep = nctimestep, filename = filenameTotalRunoff)
    ! pack variables
    nTimeSteps = size(L1_data, 3)
    allocate(L1_data_packed(level1(iDomain)%nCells, nTimeSteps))
    do tt = 1, nTimeSteps
       ! add transpose for Ulysses
      L1_data_packed(:, tt) = pack(transpose(L1_data(:, :, tt)), mask = level1(iDomain)%mask)
    end do
    !print *, 'ULYSSES mod: total runoff is multiplied by -1 to have correct sign'
    !L1_data_packed = L1_data_packed * (-1._dp)
    ! free space immediately
    deallocate(L1_data)

    ! convert if ALMA conventions have been given
    if (ALMA_convention) then
      ! convert from kg m-2 s-1 to mm TST-1
      ! 1 kg m-2 -> 1 mm depth
      ! multiply with time to obtain per timestep
      L1_data_packed = L1_data_packed * timestep * HourSecs
    end if

    ! mask lake cells that need to be lakeed
    lake_mask = (.not. transpose(data_mask(:, :, 1)) .and. level1(iDomain)%mask)

    ! read PRE and PET
    call read_forcing_nc(trim(dirTotalRunoff(iDomain)), level1(iDomain)%nrows, level1(iDomain)%ncols, &
         varnamePreRunoff, level1(iDomain)%mask, L1_data, target_period = simPer(iDomain), &
         nctimestep = nctimestep, filename = filenamePreRunoff)
    call read_forcing_nc(trim(dirTotalRunoff(iDomain)), level1(iDomain)%nrows, level1(iDomain)%ncols, &
         varnamePETRunoff, level1(iDomain)%mask, L1_data_2, target_period = simPer(iDomain), &
         nctimestep = nctimestep, filename = filenamePETRunoff)
    ! pre - pet bounded to zero
    L1_data = max(L1_data - L1_data_2, 0._dp)
    ! print *, count(pack(transpose(L1_data(:, :, tt)), mask=lake_mask) .lt. 0._dp)
    ! print *, 'before: ', count(L1_data_packed(:, 1) .lt. 0._dp), shape(L1_data_packed), minval(L1_data_packed), maxval(L1_data_packed)
    do tt = 1, nTimeSteps
       ! pack meteo values to fill lake wholes
       L1_data_packed_2 = pack(transpose(L1_data(:, :, tt)), mask=level1(iDomain)%mask)
       ! substitute meteo values in runoff data
       L1_data_packed(:, tt) = merge(L1_data_packed_2(:), L1_data_packed(:, tt), pack(lake_mask, level1(iDomain)%mask))
    end do
    ! check
    if (count(L1_data_packed .lt. 0._dp) .gt. 0_i4) then
       print *, '***ERROR: mrm_read_total_runoff: negative values in total runoff after lakes have been filled!'
       stop 1
    end if
    ! print *, 'after:  ', count(L1_data_packed(:, 1) .lt. 0._dp), shape(L1_data_packed), minval(L1_data_packed), maxval(L1_data_packed)

    ! append
    call append(L1_total_runoff_in, L1_data_packed(:, :), nodata_dp)

    !free space
    deallocate(L1_data_packed, data_mask, L1_data, L1_data_2, L1_data_packed_2)

  end subroutine mrm_read_total_runoff

  subroutine mrm_read_bankfull_runoff(iDomain)
  ! ---------------------------------------------------------------------------

  !      NAME
  !          mrm_read_bankfull_runoff

  !>         \brief reads the bankfull runoff for approximating the channel widths

  !>         \details reads the bankfull runoff, which can be calculated with
  !>         the script in mhm/post_proc/bankfull_discharge.py
  
  !     INTENT(IN)
  !>        \param[in] "integer(i4)               :: iDomain"  domain id

  !     INTENT(INOUT)
  !         None

  !     INTENT(OUT)
  !         None

  !     INTENT(IN), OPTIONAL
  !         None

  !     INTENT(INOUT), OPTIONAL
  !         None

  !     INTENT(OUT), OPTIONAL
  !         None

  !     RETURN
  !         None

  !     RESTRICTIONS
  !>        \note The file read in must contain a double precision float variable with the name
  !>        "Q_bkfl".

  !     EXAMPLE
  !         None

  !     LITERATURE
  !         None

  !     HISTORY
  !         \author Lennart Schueler
  !         \date    May 2018

    use mo_mrm_global_variables, only: level11
    use mo_read_forcing_nc, only: read_const_forcing_nc
    use mo_mrm_global_variables, only: &
         dirBankfullRunoff, &   ! directory of bankfull_runoff file for each domain
         L11_bankfull_runoff_in ! bankfull runoff at L1

    implicit none

    ! input variables
    integer(i4), intent(in) :: iDomain

    ! local variables
    logical, dimension(:,:), allocatable :: mask
    real(dp), dimension(:,:), allocatable :: L11_data ! read data from file
    real(dp), dimension(:), allocatable :: L11_data_packed

    call read_const_forcing_nc(trim(dirBankfullRunoff(iDomain)), &
                               level11(iDomain)%nrows, &
                               level11(iDomain)%ncols, &
                               "Q_bkfl", mask, L11_data)

    allocate(L11_data_packed(level11(iDomain)%nCells))
    L11_data_packed(:) = pack(L11_data(:,:), mask=level11(iDomain)%mask)

    ! append
    if (allocated(L11_bankfull_runoff_in)) then
        L11_bankfull_runoff_in = [L11_bankfull_runoff_in, L11_data_packed]
    else
        allocate(L11_bankfull_runoff_in(size(L11_data_packed)))
        L11_bankfull_runoff_in = L11_data_packed
    end if

    deallocate(L11_data)
    deallocate(L11_data_packed)

  end subroutine mrm_read_bankfull_runoff

  !    NAME
  !        rotate_fdir_variable

  !    PURPOSE
  !>       \brief TODO: add description

  !>       \details TODO: add description

  !    INTENT(INOUT)
  !>       \param[inout] "integer(i4), dimension(:, :) :: x"

  !    HISTORY
  !>       \authors L. Samaniego & R. Kumar

  !>       \date Jun 2018

  ! Modifications:
  ! Robert Schweppe Jun 2018 - refactoring and reformatting

  subroutine rotate_fdir_variable(x)

    use mo_common_constants, only : nodata_i4

    implicit none

    integer(i4), dimension(:, :), intent(INOUT) :: x


    ! 0  -1   0 |
    integer(i4) :: i, j


    !-------------------------------------------------------------------
    ! NOTE:
    !
    !     Since the DEM was transposed from (lat,lon) to (lon,lat), i.e.
    !     new DEM = transpose(DEM_old), then
    !
    !     the flow direction X (which was read) for every i, j cell needs
    !     to be rotated as follows
    !
    !                 X(i,j) = [R] * {uVector}
    !
    !     with
    !     {uVector} denoting the fDir_old (read) in vector form, and
    !               e.g. dir 8 => {-1, -1, 0 }
    !     [R]       denting a full rotation matrix to transform the flow
    !               direction into the new coordinate system (lon,lat).
    !
    !     [R] = [rx][rz]
    !
    !     with
    !           |      1       0      0 |
    !     [rx] =|      0   cos T  sin T | = elemental rotation along x axis
    !           |      0  -sin T  cos T |
    !
    !           |  cos F   sin F      0 |
    !     [rz] =| -sin F   cos F      0 | = elemental rotation along z axis
    !           |      0       0      1 |
    !
    !     and T = pi, F = - pi/2
    !     thus
    !          |  0  -1   0 |
    !     [R] =| -1   0   0 |
    !          |  0   0  -1 |
    !     making all 8 directions the following transformation were
    !     obtained.
    !-------------------------------------------------------------------
    do i = 1, size(x, 1)
      do j = 1, size(x, 2)
        if (x(i, j)  .eq. nodata_i4) cycle
        select case (x(i, j))
        case(1)
          x(i, j) = 4
        case(2)
          x(i, j) = 2
        case(4)
          x(i, j) = 1
        case(8)
          x(i, j) = 128
        case(16)
          x(i, j) = 64
        case(32)
          x(i, j) = 32
        case(64)
          x(i, j) = 16
        case(128)
          x(i, j) = 8
        end select
      end do
    end do

  end subroutine rotate_fdir_variable

end module mo_mrm_read_data
